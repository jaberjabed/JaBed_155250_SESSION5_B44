<?php

echo "How are \"You\" <br>";


$myStr = addslashes('Hello World');
echo $myStr . "<br>";

$myAdd = addslashes('Hello \37\ Hello \'37\' Hello "37" ');

echo $myAdd;
echo "<br><br>";

$myStr = "Hello World! How is life?"."<br>";

// Explode =
$myArr =  explode(" ",$myStr);
echo "<pre>";
print_r($myArr);
echo "</pre>";
echo "<br>";

// Implode =
$myStr = implode(" * ",$myArr);
echo $myStr;
echo "<br><br>";


// How to print "<br> means line break" Now we use a function called htmlentities

$myStr = '<br> means line break';
echo htmlentities($myStr);
echo "<br><br>";


// Trim = who is remove whitespace and predifine character from both side

$myStr = "       Hello World!         ";
echo trim($myStr);
echo "<br>";

//ltrim = remove from left side
$myStr = "    Hello World!          ";
echo ltrim($myStr);
echo "<br>";

//rtrim = remove from right side
$myStr = "       Hello World!       ";
echo rtrim($myStr);
echo "<br>";

//NL2br = new line type htlm \n convert to br

$myStr = "one line than \n another line";
echo nl2br($myStr);
echo "<br>";

// str_Pad

$mainStr = "Hello World";
echo str_pad($mainStr,50,'*');
echo "<br><br>";

// str_repeat
$mainStr = "Hello World<br>";
echo str_repeat($mainStr,5);
echo "<br>";

//str_replace
$mainStr = "Hello World";
echo str_replace("World","Jabed",$mainStr);
echo "<br><br>";

//str_split
$mainStr = "Hello World";
echo "<pre>";
print_r(str_split($mainStr));
echo "</pre>";
echo "<br><br>";

//strlen = print how much lenght is it
$mainStr = "Hello World";
echo strlen($mainStr);
echo "<br><br>";

//strTolower
$mainStr = "Hello World";
echo strtolower($mainStr);
echo "<br><br>";

//strTpUpper
$mainStr = "Hello World";
echo strtoupper($mainStr);
echo "<br><br>";

//subStr_compare
$mainStr = "Hello World";
$subStr = "Hello";
echo substr_compare($mainStr,$subStr,0);
echo "<br><br>";

$mainStr = "Hello World";
$subStr = "Hello World";
echo substr_compare($mainStr,$subStr,0);
echo "<br><br>";

$mainStr = "Hello World";
$subStr = "World";
echo substr_compare($mainStr,$subStr,6);
echo "<br><br>";

//subStr_count
$mainStr = "Hello World Hello World Hello World Hello World ";
$subStr = "World";
echo substr_count($mainStr,$subStr);
echo "<br><br>";

//subStr_replace
$mainStr = "Hello World Hello World Hello World Hello World ";
$subStr = "World";
echo substr_replace($mainStr,$subStr,12);
echo "<br><br>";

//ucfirst is converst the first character to uppercase
$mainStr = "hello world hello world hello world hello world ";
echo ucfirst($mainStr);
echo "<br><br>";

//ucworld which is convert the every word first character uppercase
$mainStr = "hello world hello world hello world hello world ";
echo ucwords($mainStr);
echo "<br><br>";