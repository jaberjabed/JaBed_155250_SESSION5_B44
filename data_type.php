<?php


//Boolean Example Started from here

$decision = true;
if($decision) echo "The decision is True";

$decision = false;
if($decision) echo "The decision is False <br>";

//Boolean Example ended here


//intiger Example Started from here

echo "<br><br>";
$value = 100;
echo $value. "<br>";

//intiger Example ended here



//Float/Double Example Started from here
echo "<br>";
$value = 45.20;
echo $value. "<br>";

//Float/Double Example ended here

//String Example Started from here

    $var = 100;

    $string1 = 'This is single quoted string $var <br><br>';
    echo "$string1";

    $string2 = "This is double quoted string $var <br><br><br>";

    echo "$string2";


//String Example ended here


$heredocString=<<<BITM

        This is heredoc example line 1      $var <br>
        This is heredoc example line 2      $var <br>
        This is heredoc example line 3      $var <br>
        This is heredoc example line 4      $var <br>
        This is heredoc example line 5      $var <br>

BITM;


$nowdocString=<<<'BITM'

        This is nowdoc example line 1       $var <br>
        This is nowdoc example line 2       $var <br>
        This is nowdoc example line 3       $var <br>
        This is nowdoc example line 4       $var <br>
        This is nowdoc example line 5       $var <br>

BITM;

echo $heredocString . "<br> <br>" . $nowdocString;

$arr=array(1,2,3,4,5,6,7,8,9,10);
print_r($arr);

echo "<br><br>";

$cars=array("BMW","Volvo","Toyota","NISSAN","FORD","FERAREE","MARUTI");
print_r($cars);

echo "<br><br>";

$ageArray = array("Rubel"=>25,"Arif"=>22,"Shakil"=>25,"Riaz"=>23);
print_r($ageArray);
echo "<br><br>";
echo "The age of Ruble is  " . $ageArray ["Rubel"];

